var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var DONE = false;
// プレーヤーの埋め込み箇所
var player;
// プレーヤーのサイズ
var ytWidth = 560;
var ytHeight = 315;

console.log(DONE);


//    after the API code downloads.
// プレーヤーの埋め込み
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: ytHeight,
        width: ytWidth,
        videoId: 'dfESDQNkFj0',
        playerVars: {
            rel: 0, // 再生終了後に関連動画を表示するかどうか設定
            // autoplay: 1, // 自動再生するかどうか設定
            controls: 0, // コントロールバー
            disablekb: 1, // キーボードでの操作をさせない
            loop: 1, // ループの設定
            playlist: 'dfESDQNkFj0', // 再生する動画のリスト

        },
        events: {
            'onReady': onPlayerReady,
        }
    });
}

// プレーヤー準備完了後の処理
function onPlayerReady(event) {
    event.target.mute()
}


$(function() {
    window.addEventListener("scroll", function() {
        var playre_top = $('#player').offset().top;
        var scrollTop = $(window).scrollTop();
        var WH = Math.floor($(window).height() / 2);

        // 画面内にプレイヤーがあるかを判定
        if ((playre_top < (scrollTop + WH)) && ((scrollTop + WH < playre_top + ytHeight))) {
            // 動画再生
            if (player.getPlayerState() !== 1) {
                player.playVideo();
            }
        } else {
            // 動画停止
            if (player.getPlayerState() == 1) {
                player.pauseVideo();
            }
        }

    })
});